﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : MonoBehaviour {

    [SerializeField]
    protected string[] dialog;
    [SerializeField]
    protected bool startActive = false;
    
    protected bool active;

    private static DialogManager dialogManager;

    protected void Awake() {
        if(!dialogManager) {
            dialogManager = FindObjectOfType<DialogManager>();
        }
        active = startActive;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(active && collision.tag == "player") {
            dialogManager.Dialog = dialog;
            active = false;
        }
    }

}
