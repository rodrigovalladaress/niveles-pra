﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimit : MonoBehaviour {

    public enum TypeEnum {
        horizontal,
        vertical
    }

    [SerializeField]
    private TypeEnum type;

    private Renderer rend;
    private static AvoidLimits avoidLimits;

    public TypeEnum Type {
        get {
            return type;
        }

        set {
            type = value;
        }
    }

    private void Awake() {
        rend = GetComponent<Renderer>();
        avoidLimits = avoidLimits ? 
            avoidLimits : FindObjectOfType<AvoidLimits>();
    }

    private void OnBecameVisible() {
        avoidLimits.AddLimit(this);
    }

    private void OnBecameInvisible() {
        avoidLimits.RemoveLimit(this);
    }

}
