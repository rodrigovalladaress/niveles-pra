﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// PlayerLifeUI se encarga de la gestión de la GUI con la vida del personaje.
/// </summary>
public class PlayerLifeUI : MonoBehaviour {

    [SerializeField]
    private GameObject lifeGUI;

    [SerializeField]
    private Sprite[] lifeSprites;

    public void ChangeLife(int life) {
        if (life > 0) {
            lifeGUI.GetComponent<Image>().sprite = lifeSprites[life - 1];
        }
        else {
            lifeGUI.GetComponent<Image>().sprite = lifeSprites[0];
        }
    }

}
