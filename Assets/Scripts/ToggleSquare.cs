﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSquare : MonoBehaviour {

    public enum SquareColor {
        blue,
        green,
        red,
        yellow,
        violet
    }

    [SerializeField]
    private SquareColor color;

    public SquareColor Color {
        get {
            return color;
        }
    }
}
