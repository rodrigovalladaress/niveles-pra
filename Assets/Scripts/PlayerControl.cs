﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// PlayerControl se encarga del control y el comportamiento del jugador.
/// </summary>
public class PlayerControl : CharacterControl {

    private PlayerLifeUI playerLifeUI;
    private AudioSource audioSrc;

    private GameManager gameMngr;

    [SerializeField]
    private float invincibleSeconds;
    [SerializeField]
    private AudioClip hittedSound;
    [SerializeField]
    private bool allowMovement = true;

    public bool AllowMovement {
        get {
            return allowMovement;
        }

        set {
            allowMovement = value;
        }
    }

    private void Awake() {
        playerLifeUI = GetComponent<PlayerLifeUI>();
        audioSrc = GetComponent<AudioSource>();
        gameMngr = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

	private void Update () {
        if(allowMovement) {
            bool jump = Input.GetButtonDown(InputKeyNames.JUMP);
            bool jumpHeld = Input.GetButton(InputKeyNames.JUMP);
            bool shoot = Input.GetButtonDown(InputKeyNames.FIRE) 
                || Input.GetButton(InputKeyNames.FIRE);
            float direction = Input.GetAxis(InputKeyNames.HORIZONTAL);
            if (stateCtrl.State != StateControl.CharacterState.dead) {
                if (shoot && stateCtrl.CanShootNow) {
                    shootCtrl.Shoot();
                }
                else if (stateCtrl.State != StateControl.CharacterState.shooting) {
                    motor.normalizedXMovement = direction;
                    if (jump) {
                        motor.Jump();
                    }
                    motor.jumpingHeld = jumpHeld;
                }
                else {
                    motor.normalizedXMovement = 0;
                }
            }
            else {
                motor.normalizedXMovement = 0;
            }
        } else {
            motor.normalizedXMovement = 0;
        }
    }

    public override void Die() {
        StartCoroutine("DieCoroutine");
    }

    private IEnumerator DieCoroutine() {
        stateCtrl.Dead = true;
        yield return new WaitForSeconds(1f);
        gameMngr.MovePlayerToLastSpawnPoint();
        yield return new WaitForSeconds(0.1f);
        stateCtrl.Dead = false;
    }

    public override void OnLifeChanged(int currentLife, int lifeChange, GameObject source) {
        playerLifeUI.ChangeLife(currentLife);
        if (stateCtrl.State != StateControl.CharacterState.dead) {
            if (lifeChange < 0) {
                audioSrc.clip = hittedSound;
                audioSrc.Play();
                motor.Dash(Vector2.right * stateCtrl.DirectionFacing * -1);
                StartCoroutine("SetInvincible");
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == Tag.DIAMOND) {
            other.GetComponent<DiamondControl>().DiamondGot();
        }
    }

    private IEnumerator SetInvincible() {
        stateCtrl.Invincible = true;
        yield return new WaitForSeconds(invincibleSeconds);
        stateCtrl.Invincible = false;
    }

    public void RestoreLife() {
        damageCtrl.RestoreLife();
    }

}
