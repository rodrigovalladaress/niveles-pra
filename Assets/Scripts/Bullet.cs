﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Comportamiento de las balas.
/// </summary>
public class Bullet : MonoBehaviour {

    public enum CharacterType {
        player,
        enemy
    }

    private float direction;
    public static Transform bulletsParent;

    [SerializeField]
    private int damage;

    [SerializeField]
    private CharacterType harmThisCharacters;
    [SerializeField]
    private string[] explodeToThisLayers;

    [SerializeField]
    private GameObject explosionPrefab;

    private GameObject explosion;

    public float Direction {
        get { return direction; }
        set { direction = value; }
    }

    void Awake() {
        if (explosionPrefab) {
            explosion = Instantiate(explosionPrefab);
            explosion.SetActive(false);
            explosion.transform.SetParent(gameObject.transform);
            explosion.transform.localPosition = Vector2.zero;
        }
    }

    // Use this for initialization
    void Start() {
        if (!bulletsParent) {
            bulletsParent = GameObject.Find("Bullets").transform;
        }
    }

    public void OnCollisionEnter2D(Collision2D other) {
        CheckCollision(other.gameObject);
    }

    public void OnTriggerEnter2D(Collider2D other) {
        CheckCollision(other.gameObject);
    }

    private void CheckCollision(GameObject other) {
        if (other.gameObject.layer == Layers.CHARACTER || other.gameObject.layer == Layers.GROUND || other.gameObject.layer == Layers.ENEMY) {
            if (other.tag.ToLower() == harmThisCharacters.ToString().ToLower()) {
                other.GetComponent<DamageControl>().Harm(damage);
            }
            Explode();
        }
    }

    private void Explode() {
        float secsToDestroy = 0f;
        if (explosion) {
            Animator expAnim = explosion.GetComponent<Animator>();
            RuntimeAnimatorController expAnimCtrler;
            AudioSource expAudioSrc = explosion.GetComponent<AudioSource>();
            explosion.SetActive(true);
            if (expAnim) {
                expAnimCtrler = expAnim.runtimeAnimatorController;
                
                foreach (AnimationClip animClip in expAnimCtrler.animationClips) {
                    secsToDestroy += animClip.averageDuration;
                }
            }
            if (expAudioSrc) {
                float secsExpAudio = expAudioSrc.clip.length;
                if (secsExpAudio > secsToDestroy) {
                    secsToDestroy = secsExpAudio;
                }
            }
        }
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        // Evitar que la bala se siga moviendo después de que explote
        GetComponent<PlatformerMotor2D>().enabled = false;
        Invoke("DestroyGameObject", secsToDestroy);
    }

    private void DestroyGameObject() {
        Destroy(gameObject);
    }

}
