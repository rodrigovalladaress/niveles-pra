﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Gestión del movimiento de la cámara.
/// </summary>
public class CameraControl : MonoBehaviour {

    private enum FreezePositionEnum {
        horizontal,
        vertical,
        none
    }

    [SerializeField]
    private FreezePositionEnum freezePosition;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private float maxCameraX;

    private float cameraX;
    private float cameraY;
    private float cameraZ;

    private float minCameraX;
    private float minCameraY;

    void Start () {
        cameraX = transform.position.x;
        cameraY = transform.position.y;
        cameraZ = transform.position.z;
        minCameraX = transform.position.x;
        minCameraY = transform.position.y;
    }

	void Update () {
        if (player) {
            float playerX = player.transform.position.x;
            float playerY = player.transform.position.y;

            if (freezePosition == FreezePositionEnum.vertical) {
                if (playerX >= minCameraX && playerX <= maxCameraX) {
                    transform.position = new Vector3(playerX, cameraY, cameraZ);
                }
            }
            else if (freezePosition == FreezePositionEnum.horizontal) {
                if (playerY >= minCameraY) {
                    transform.position = new Vector3(cameraX, playerY, cameraZ);
                }
            } else {
                transform.position = new Vector3(playerX, playerY, cameraZ);
            }
        }
	}
}
