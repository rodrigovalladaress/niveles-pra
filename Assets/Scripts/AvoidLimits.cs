﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidLimits: MonoBehaviour {

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float epsilon = 1f;
    [SerializeField]
    private float delta = 0.2f;
    [SerializeField]
    private float distanceToMoveSmooth = 0.2f;

    private List<CameraLimit> limits;
    private float initialZ;
    float halfHeight;
    float halfWidth;
    bool moving = false;
    bool waiting = true;

    private void Awake() {
        Camera camera = GetComponent<Camera>();
        halfHeight = camera.orthographicSize;
        halfWidth = camera.aspect * halfHeight + epsilon;
        halfHeight += epsilon;

        limits = new List<CameraLimit>();
        initialZ = transform.position.z;

        StartCoroutine(WaitFirstFrame());
    }

    public void AddLimit(CameraLimit limit) {
        limits.Add(limit);
    }

    public void RemoveLimit(CameraLimit limit) {
        limits.Remove(limit);
    }

    private IEnumerator WaitFirstFrame() {
        waiting = true;
        yield return new WaitForEndOfFrame();
        waiting = false;
    }

    private IEnumerator MoveCameraSmoothly(Vector3 to, float percentage = 0f) {
        Vector3 from = transform.position;
        moving = true;
        while(percentage < 1f) {
            Vector3 newPos = Vector3.Lerp(from, to, percentage);
            transform.position = newPos;
            yield return new WaitForEndOfFrame();
            percentage += delta;
        }
        moving = false;
    }

    private void Update() {
        if (!moving && player && !waiting) {
            float playerX = player.transform.position.x;
            float playerY = player.transform.position.y;

            float topLimit = float.NegativeInfinity;
            float botLimit = float.PositiveInfinity;
            float leftLimit = float.PositiveInfinity;
            float rightLImit = float.NegativeInfinity;

            float newX = transform.position.x;
            float newY = transform.position.y;

            Vector3 newPos;
            float distanceBtwPos;

            foreach (var limit in limits) {
                float x = limit.transform.position.x;
                float y = limit.transform.position.y;
                if(limit.Type == CameraLimit.TypeEnum.vertical) {
                    if (x < leftLimit) {
                        leftLimit = x;
                    } else if (x > rightLImit) {
                        rightLImit = x;
                    }
                } else if(limit.Type == CameraLimit.TypeEnum.horizontal) {
                    if (y < botLimit) {
                        botLimit = y;
                    } else if (y > topLimit) {
                        topLimit = y;
                    }
                }
            }

            if(topLimit == float.NegativeInfinity) {
                topLimit = float.PositiveInfinity;
            }
            if(botLimit == float.PositiveInfinity) {
                botLimit = float.NegativeInfinity;
            }
            if(leftLimit == float.PositiveInfinity) {
                leftLimit = float.NegativeInfinity;
            }
            if(rightLImit == float.NegativeInfinity) {
                rightLImit = float.PositiveInfinity;
            }


            if(playerX - halfWidth >= leftLimit && playerX + halfWidth <= rightLImit) {
                newX = playerX;
            }
            if(playerY - halfHeight >= botLimit && playerY + halfHeight <= topLimit) {
                newY = playerY;
            }
            newPos = new Vector3(newX, newY, initialZ);
            distanceBtwPos = Vector3.Distance(newPos, transform.position);

            if(distanceBtwPos > 0f) {
                if (distanceBtwPos >= distanceToMoveSmooth) {
                    StartCoroutine(MoveCameraSmoothly(newPos));
                } else {
                    transform.position = newPos;
                }
            }
        }
    }
}
