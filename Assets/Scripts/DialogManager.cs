﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour {

    [SerializeField]
    private Text text;
    [SerializeField]
    private KeyCode[] keysToNextLine = 
        {
            KeyCode.Return,
            KeyCode.KeypadEnter
        };

    private bool showingDialog;
    private string[] dialog;
    private int currentDialog = 0;
    private PlayerControl player;

    public string[] Dialog {
        get {
            return dialog;
        }
        set {
            dialog = value;
            Show();
        }
    }

    private void Awake() {
        player = FindObjectOfType<PlayerControl>();
        Hide();
    }

    private void NextLine() {
        currentDialog++;
        ShowLine();
    }

    private void ShowLine() {
        if (currentDialog < dialog.Length) {
            text.text = dialog[currentDialog];
        } else {
            Hide();
        }
    }

    private void Hide() {
        currentDialog = 0;
        showingDialog = false;
        text.gameObject.SetActive(false);
        player.AllowMovement = true;
    }

    private void Show() {
        currentDialog = 0;
        showingDialog = true;
        text.gameObject.SetActive(true);
        player.AllowMovement = false;
        ShowLine();
    }

    private bool CheckKeyDown() {
        bool anyPressed = false;
        int i = 0;
        
        while(!anyPressed && i < keysToNextLine.Length) {
            anyPressed = Input.GetKeyDown(keysToNextLine[i]);
            i++;
        }
        return anyPressed;
    }
	
	private void Update () {
		if(showingDialog && CheckKeyDown()) {
            NextLine();
        }
	}
}
