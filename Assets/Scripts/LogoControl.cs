﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Animación del logo de la pantalla de logo.
/// </summary>
public class LogoControl : MonoBehaviour {

    private float fadeTime;
    private GameManager gameMngr;

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float loadMenuDelay;

	void Awake () {
        player.SetActive(false);
        gameMngr = GameObject.Find("GameManager").GetComponent<GameManager>();
        fadeTime = GetComponent<FadeObjectInOut>().fadeTime / 2;
        StartCoroutine("LogoCoroutine");
	}

    private IEnumerator LogoCoroutine() {
        yield return new WaitForSeconds(fadeTime);
        player.SetActive(true);
        yield return new WaitForSeconds(loadMenuDelay);
        gameMngr.LoadTitle();
    }

}
