﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
/// Control del flujo de la partida.
/// </summary>
public class GameManager : MonoBehaviour {

    public const string TITLE = "Title";
    public const string DEFAULT_LEVEL = "1";

    [SerializeField]
    private GameObject startPoint;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject fadeBetweenScenes; // Efecto de "cortinilla" negra cuando se cambia de 
                                          // escena.

    private GameObject currentSpawnPoint; // Lugar donde se moverá al jugador si este muere.

    private string nextScene;

    private float fadeTimeBetweenScenes;

    public GameObject CurrentSpawnPoint {
        get { return currentSpawnPoint; }
        set { currentSpawnPoint = value; }
    }

    void Start() {
        GameObject musicMngr = GameObject.Find("MusicManager");
        if (startPoint) {
            currentSpawnPoint = startPoint;
        }
        if (musicMngr) {
            DontDestroyOnLoad(musicMngr); // Para que la música se siga escuchando aunque se cambie
                                          // de escena.
        }
        if (fadeBetweenScenes) {
            fadeTimeBetweenScenes = fadeBetweenScenes.GetComponent<FadeObjectInOut>().fadeTime;
        }
    }

    public void MovePlayerToLastSpawnPoint() {
        player.transform.position = currentSpawnPoint.transform.position;
    }

    public void LoadTitle() {
        LoadScene(TITLE);
    }

    public void LoadDefaultScene() {
        LoadScene(DEFAULT_LEVEL);
    }

    public void LoadScene(string scene) {
        nextScene = scene;
        StartCoroutine("LoadSceneCoroutine");
    }

    private IEnumerator LoadSceneCoroutine() {
        if (fadeBetweenScenes) {
            fadeBetweenScenes.GetComponent<FadeObjectInOut>().enabled = true;
            yield return new WaitForSeconds(fadeTimeBetweenScenes);
        }
        SceneManager.LoadScene(nextScene);
    }

    public void ExitGame() {
        Application.Quit();
    }

}
