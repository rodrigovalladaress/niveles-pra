﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Se encarga de la gestión de la vida de los personajes (daño y recuperar vida).
/// </summary>
public class DamageControl : MonoBehaviour {

    private CharacterControl charCtrl;
    private StateControl stateCtrl;

    [SerializeField]
    private int life;
    private int startingLife;

    [SerializeField]
    private int power;

    public int Life {
        get { return life; }
    }

    public int Power {
        get { return power; }
    }

    void Start () {
        charCtrl = GetComponent<CharacterControl>();
        stateCtrl = GetComponent<StateControl>();
        startingLife = life;
	}

    public void RestoreLife() {
        int prevLife = life;
        life = startingLife;
        charCtrl.OnLifeChanged(life, life - prevLife, gameObject);
    }

    public void Harm(int damage) {
        if ((!charCtrl.CanBeInvincible || (charCtrl.CanBeInvincible && !stateCtrl.Invincible)) 
            && stateCtrl.State != StateControl.CharacterState.dead) {
            life -= damage;
            charCtrl.OnLifeChanged(life, -damage, gameObject);
            if (life <= 0) {
                charCtrl.Die();
            }
        }
    }

}
