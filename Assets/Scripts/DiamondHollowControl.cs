﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Hueco donde se colocan los diamantes.
/// </summary>
public class DiamondHollowControl : MonoBehaviour {

    private DiamondDoorControl diamondDoorCtrl;
    private SpriteRenderer spriteRenderer;

    void Start() {
        diamondDoorCtrl = transform.parent.GetComponent<DiamondDoorControl>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void DiamondAcquired() {
        diamondDoorCtrl.DiamondAcquired();
        ChangeSpriteToAcquired();
    }

    private void ChangeSpriteToAcquired() {
        spriteRenderer.sprite = diamondDoorCtrl.DiamondAcquiredSprite;
    }

}
