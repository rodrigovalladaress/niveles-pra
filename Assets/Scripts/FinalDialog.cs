﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDialog : Dialog {

    [SerializeField]
    private int numButtons = 3;
    
    private int numButtonsActive = 0;

    private new void Awake() {
        startActive = false;
        base.Awake();
    }

    public void ButtonActivated() {
        numButtonsActive++;
        if(numButtonsActive == numButtons) {
            active = true;
        }
    }

}
