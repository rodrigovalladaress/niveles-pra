﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSquaresButton : MonoBehaviour {

    [SerializeField]
    private ToggleSquare.SquareColor color;

    private ToggleSquare[] squares;

    private static FinalDialog finalDialog;

    private void Awake() {
        if(!finalDialog) {
            finalDialog = FindObjectOfType<FinalDialog>();
        }
    }

    void Start () {
        ToggleSquare[] allSquares = FindObjectsOfType<ToggleSquare>();
        List<ToggleSquare> listSquares = new List<ToggleSquare>();
        foreach(var s in allSquares) {
            if(s.Color == color) {
                listSquares.Add(s);
            }
        }
        squares = listSquares.ToArray();
	}

    public void OnTriggerEnter2D(Collider2D other) {
        foreach (var s in squares) {
            s.gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
        finalDialog.ButtonActivated();
    }

}
